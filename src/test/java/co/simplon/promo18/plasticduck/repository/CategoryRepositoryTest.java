package co.simplon.promo18.plasticduck.repository;


import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import co.simplon.promo18.plasticduck.entity.Category;

@SpringBootTest
public class CategoryRepositoryTest {
@Autowired
CategoryRepository repo;

  @Test
  void testFindAll() {
    List<Category> result = repo.findAll();
    assertEquals(2,result.size());

  }
}
