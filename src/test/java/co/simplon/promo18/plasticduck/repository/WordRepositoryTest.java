package co.simplon.promo18.plasticduck.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import co.simplon.promo18.plasticduck.entity.Word;

@SpringBootTest
public class WordRepositoryTest {
  @Autowired
  private WordRepository repo;

  @Test
  void testFindAll() {
    List<Word> result = repo.findAll();
    assertEquals(3, result.size());

  }
}
