package co.simplon.promo18.plasticduck.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.plasticduck.entity.Category;

@Repository
public class CategoryRepository {
  @Autowired
  private DataSource dataSource;

  public List<Category> findAll(){
    List<Category> cList= new ArrayList<>();

    try (Connection connection = dataSource.getConnection();) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM category");
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        Category category = new Category(rs.getInt("id"), rs.getString("label"));
        cList.add(category);

    }
   } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
    return cList;
  }
}
