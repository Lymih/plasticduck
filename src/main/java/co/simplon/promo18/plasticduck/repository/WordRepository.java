package co.simplon.promo18.plasticduck.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.plasticduck.entity.Category;
import co.simplon.promo18.plasticduck.entity.Word;

@Repository
public class WordRepository {
  @Autowired
  private DataSource dataSource;

  public List<Word> findAll(){
    List<Word> wList = new ArrayList<>();

    try(Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM word LEFT JOIN category ON word.id_category = category.id");
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        Category category = new Category(rs.getInt("category.id"), rs.getString("category.label"));
        Word word = new Word(rs.getInt("id"),rs.getString("word.label"),category);
        wList.add(word);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
    return wList;
  }
}

