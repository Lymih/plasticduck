package co.simplon.promo18.plasticduck.entity;

public class Word {
  private Integer id;
  private String label;
  private Category category;
  
  public Word() {}
  public Word(String label, Category category) {
    this.label = label;
    this.category = category;
  }
  public Word(Integer id, String label, Category category) {
    this.id = id;
    this.label = label;
    this.category = category;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getLabel() {
    return label;
  }
  public void setLabel(String label) {
    this.label = label;
  }
  public Category getCategory() {
    return category;
  }
  public void setCategory(Category category) {
    this.category = category;
  }
  
}
