package co.simplon.promo18.plasticduck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlasticduckApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlasticduckApplication.class, args);
	}

}
