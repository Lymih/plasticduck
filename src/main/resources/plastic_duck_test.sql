
DROP DATABASE if EXISTS plastic_duck;

CREATE DATABASE IF NOT EXISTS plastic_duck;

USE plastic_duck;

CREATE TABLE category (
  id INT AUTO_INCREMENT PRIMARY KEY,
  label VARCHAR (24) NOT NULL
);

CREATE TABLE word (
  id INT AUTO_INCREMENT PRIMARY KEY,
  label VARCHAR(24) NOT NULL,
  id_category INT NOT NULL,
  CONSTRAINT word_category FOREIGN KEY (id_category) REFERENCES category(id)
);
USE plastic_duck;

INSERT INTO category (label) VALUES (
  'nourriture'),('nature');

INSERT INTO word (label,id_category) VALUES ('pain',1),
('arbre',2),
('pomme',1);