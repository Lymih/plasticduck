DROP DATABASE IF EXISTS plastic_duck;

CREATE DATABASE plastic_duck;

USE plastic_duck;

CREATE TABLE category (
  id INT AUTO_INCREMENT PRIMARY KEY,
  label VARCHAR (24) NOT NULL
);

CREATE TABLE word (
  id INT AUTO_INCREMENT PRIMARY KEY,
  label VARCHAR(24) NOT NULL,
  id_category INT NOT NULL,
  CONSTRAINT word_category FOREIGN KEY (id_category) REFERENCES category(id)
);